﻿using System;

namespace Absolute
{
	class Program
	{
		static void Main(string[] args)
		{
			int x;
			Console.Write("Input value: ");
			x = int.Parse(Console.ReadLine());

			int abs = x;

			if (x < 0)
			{
				abs = -x;
			}

			Console.WriteLine($"|{x}| = {abs}");
		}
	}
}
