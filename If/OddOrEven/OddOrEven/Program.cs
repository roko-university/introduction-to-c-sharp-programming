﻿using System;

namespace OddOrEven
{
	class Program
	{
		static void Main(string[] args)
		{
			int x;
			Console.Write("Input value: ");
			x = int.Parse(Console.ReadLine());

			if (x % 2 == 0)
			{
				Console.WriteLine("Even");
			}
			else
			{
				Console.WriteLine("Odd");
			}
		}
	}
}
