﻿using System;

namespace Sign
{
	class Program
	{
		static void Main(string[] args)
		{
			int x;
			Console.Write("Input value: ");
			x = int.Parse(Console.ReadLine());

			if (x > 0)
			{
				Console.WriteLine($"{x} - positive");
			}
			else
			{
				if (x == 0)
				{
					Console.WriteLine($"{x} - zero");
				}
				else
				{
					Console.WriteLine($"{x} - negative");
				}
			}
		}
	}
}
