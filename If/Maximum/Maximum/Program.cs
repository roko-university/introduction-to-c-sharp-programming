﻿using System;

namespace Maximum
{
	class Program
	{
		static void Main(string[] args)
		{
			int x;
			Console.Write("Input first value: ");
			x = int.Parse(Console.ReadLine());

			int y;
			Console.Write("Input second value: ");
			y = int.Parse(Console.ReadLine());

			if (x > y)
			{
				Console.WriteLine($"Maximum of {x} and {y} is {x}");
			}
			else
			{
				Console.WriteLine($"Maximum of {x} and {y} is {y}");
			}
		}
	}
}
