﻿using System;

namespace MaxOfThreeNumbers
{
	class Program
	{
		static void Main(string[] args)
		{
			int x;
			Console.Write("Input first value: ");
			x = int.Parse(Console.ReadLine());

			int y;
			Console.Write("Input second value: ");
			y = int.Parse(Console.ReadLine());

			int z;
			Console.Write("Input third value: ");
			z = int.Parse(Console.ReadLine());

			int max = x;

			if(max < y)
			{
				max = y;
			}

			if(max < z)
			{
				max = z;
			}

			Console.WriteLine($"Max of {x}, {y} and {z} = {max}");

			//if(x > y && x > z)
			//{
			//	Console.WriteLine(x);
			//}
			//else
			//{
			//	if(y > z)
			//	{
			//		Console.WriteLine(y);
			//	}
			//	else
			//	{
			//		Console.WriteLine(z);
			//	}
			//}


			//if(x > y)
			//{
			//	if(x > z)
			//	{
			//		Console.WriteLine(x);
			//	}
			//	else
			//	{
			//		Console.WriteLine(z);
			//	}
			//}
			//else
			//{
			//	if(y > z)
			//	{
			//		Console.WriteLine(y);
			//	}
			//	else
			//	{
			//		Console.WriteLine(z);
			//	}
			//}
		}
	}
}
