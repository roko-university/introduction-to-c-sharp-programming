﻿using System;

namespace Factorial
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Input number ");
			int a = int.Parse(Console.ReadLine());

			int res = Factorial(a);

			Console.WriteLine($"{a}! = {res}");
		}

		static int FactorialLoop(int n)
		{
			int res = 1;
			for(int i = 2; i <= n; i++)
			{
				res *= i;
			}
			return res;
		}

		static int Factorial(int n)
		{
			if (n == 1 || n == 0)
				return 1;

			return n * Factorial(n - 1);
		}
	}
}
