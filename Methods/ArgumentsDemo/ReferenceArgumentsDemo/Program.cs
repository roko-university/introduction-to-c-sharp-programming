﻿using System;

namespace ReferenceArgumentsDemo
{
	class Program
	{
		static void PositiveFilter(ref int x)
		{
			if (x < 0)
			{
				x = 0;
			}
		}

		static void Swap(ref int x, ref int y)
		{
			int tmp = x;
			x = y;
			y = tmp;
		}

		static void Main(string[] args)
		{
			int x;
			int y;
			ReadData(out x, out y);

			Console.WriteLine($"x = {x}, y = {y}");
			int a = x * 2;
			Swap(ref a, ref y);
			Console.WriteLine($"x = {x}, y = {y}");

			int[] arr = { 2, -5, 5, 8, -6 };

			WriteArray(arr);
			for (int i = 0; i < arr.Length; i++)
			{
				PositiveFilter(ref arr[i]);
			}

			WriteArray(arr);
		}

		private static void ReadData(out int x, out int y)
		{
			Console.WriteLine("Input value");
			x = int.Parse(Console.ReadLine());
			Console.WriteLine("Input value");
			y = int.Parse(Console.ReadLine());
		}

		static void WriteArray(int[] array)
		{
			if (array.Length == 0)
			{
				return;
			}

			Console.Write(array[0]);
			for (int i = 1; i < array.Length; i++)
			{
				Console.Write($", {array[i]}");
			}

			Console.WriteLine();
		}
	}
}
