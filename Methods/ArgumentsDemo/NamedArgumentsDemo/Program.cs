﻿using System;

namespace NamedArgumentsDemo
{
	class Program
	{
		static void WriteArray(int[] array,
			string delimeter = ", ",
			ConsoleColor color = ConsoleColor.Green,
			bool newLine = true)
		{
			if (array.Length == 0)
			{
				return;
			}

			var oldColor = Console.ForegroundColor;
			Console.ForegroundColor = color;

			Console.Write(array[0]);
			for (int i = 1; i < array.Length; i++)
			{
				Console.Write($"{delimeter}{array[i]}");
			}

			if (newLine)
			{
				Console.WriteLine();
			}

			Console.ForegroundColor = oldColor;
		}

		static void Main(string[] args)
		{
			int[] arr = { 1, -2, -3, 4, 5 };
			WriteArray(arr);
			WriteArray(arr, ";");
			WriteArray(arr, ", ", ConsoleColor.Yellow);
			WriteArray(color:ConsoleColor.Blue, array: arr);
		}
	}
}
