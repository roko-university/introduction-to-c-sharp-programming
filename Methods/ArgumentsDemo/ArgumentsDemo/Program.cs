﻿using System;

namespace ArgumentsDemo
{
	class Program
	{
		static int Sum(int x, int y)
		{
			Console.WriteLine($"{x}, {y}");
			x = 10;
			Console.WriteLine($"{x}, {y}");
			return x + y;
		}

		static void Main(string[] args)
		{
			int x = 5;
			int y = 6;
			Console.WriteLine($"{x}, {y}");
			int result = Sum(x, Sum(2, 2));
			Console.WriteLine($"{x}, {y}, {result}");
		}
	}
}
