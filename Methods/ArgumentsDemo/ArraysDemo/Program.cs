﻿using System;

namespace ArraysDemo
{
	class Program
	{
		static void WriteArray(int[] array)
		{
			if (array.Length == 0)
			{
				return;
			}

			Console.Write(array[0]);
			for (int i = 1; i < array.Length; i++)
			{
				Console.Write($", {array[i]}");
			}

			Console.WriteLine();
		}

		static void ReplaceNegative(int[] array)
		{
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i] < 0)
				{
					array[i] = 0;
				}
			}
		}

		static void Main(string[] args)
		{
			int[] arr = { 1, -2, -3, 4, 5 };
			WriteArray(arr);
			ReplaceNegative(arr);
			WriteArray(arr);
		}
	}
}
