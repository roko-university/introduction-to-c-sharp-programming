﻿using System;

namespace DefaultArgumentsDemo
{
	class Program
	{
		static void WriteArray(int[] array, 
			char delimeter = ',')
		{
			#region method body
			if (array.Length == 0)
			{
				return;
			}

			Console.Write(array[0]);
			for (int i = 1; i < array.Length; i++)
			{
				Console.Write($"{delimeter} {array[i]}");
			}

			Console.WriteLine();
			#endregion
		}

		static void Main(string[] args)
		{
			int[] arr = { 1, -2, -3, 4, 5 };
			WriteArray(arr);
			WriteArray(arr, ';');
		}
	}
}
