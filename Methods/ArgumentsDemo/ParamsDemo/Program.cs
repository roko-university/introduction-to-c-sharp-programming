﻿using System;

namespace ParamsDemo
{
	class Program
	{
		static int Sum(params int[] array)
		{
			#region method body
			int summa = 0;
			for (int i = 0; i < array.Length; i++)
			{
				summa += array[i];
			}
			return summa;
			#endregion
		}
		static void Main(string[] args)
		{
			Console.WriteLine(Sum(5));
			Console.WriteLine(Sum(5, 5));
			Console.WriteLine(Sum(5, 5, 5));
			Console.WriteLine(Sum(5, 5, 5, 5, 5, 5, 5, 5, 5, 5));
			
			int[] arr = { 1, 2, 3, 4, 5 };
			Console.WriteLine(Sum(arr));
		}
	}
}
