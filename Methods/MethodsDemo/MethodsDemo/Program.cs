﻿using System;

namespace MethodsDemo
{
	class Program
	{
		static int Sum(int x, int y)
		{
			int a = x + y;
			return a;
		}

		static void Main(string[] args)
		{
			int a;
			int b;
			Console.Write("Input first value: ");
			a = int.Parse(Console.ReadLine());
			Console.Write("Input second value: ");
			b = int.Parse(Console.ReadLine());

			int result = Sum(a, b);

			Console.WriteLine($"Summa of {a} and {b} = {result}");
		}
	}
}
