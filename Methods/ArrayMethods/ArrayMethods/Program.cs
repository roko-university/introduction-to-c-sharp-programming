﻿using System;

namespace ArrayMethods
{
	class Program
	{
		static int[] GenerateRandomArray(int n)
		{
			int[] arr = new int[n];

			Random rnd = new Random();

			for (int i = 0; i < arr.Length; i++)
			{
				arr[i] = rnd.Next(-100, -10);
			}

			return arr;
		}

		static void Main(string[] args)
		{
			Console.WriteLine("Input array length");
			int n = int.Parse(Console.ReadLine());

			int[] arr = GenerateRandomArray(n);
			WriteArray(arr);
			int sum = Summa(arr);
			int max = Max(arr);

			Console.WriteLine($"Summa = {sum}");
			Console.WriteLine($"Maximum = {max}");
		}

		private static int Max(int[] arr)
		{
			int max = int.MinValue;
			for (int i = 0; i < arr.Length; i++)
			{
				if(max < arr[i])
				{
					max = arr[i];
				}
			}
			return max;
		}

		private static int Summa(int[] arr)
		{
			int sum = 0;

			for (int i = 0; i < arr.Length; i++)
			{
				sum += arr[i];
			}

			return sum;
		}

		private static void WriteArray(int[] arr)
		{
			for (int i = 0; i < arr.Length; i++)
			{
				Console.WriteLine(arr[i]);
			}
		}
	}
}
