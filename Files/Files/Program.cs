﻿using System;
using System.IO;

namespace Files
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Input file path ");
			string path = Console.ReadLine();

			string[] lines = File.ReadAllLines(path);

			int[] arr = ConvertToIntArray(lines);

			WriteArray(arr);

			ReplaceNegative(arr, 0);

			WriteArray(arr);

			Console.WriteLine("Array was processed. Input path to the output file. ");

			string outputPath = Console.ReadLine();

			File.WriteAllLines(outputPath, ConvertToStringArray(arr));
		}

		private static int[] ConvertToIntArray(string[] lines)
		{
			int[] arr = new int[lines.Length];
			for (int i = 0; i < lines.Length; i++)
			{
				arr[i] = int.Parse(lines[i]);
			}

			return arr;
		}

		private static string[] ConvertToStringArray(int[] lines)
		{
			string[] arr = new string[lines.Length];
			for (int i = 0; i < lines.Length; i++)
			{
				arr[i] = lines[i].ToString();
			}

			return arr;
		}

		private static void ReplaceNegative(int[] arr, int value)
		{
			for (int i = 0; i < arr.Length; i++)
			{
				if (arr[i] < 0)
					arr[i] = value;
			}
		}

		static void WriteArray(int[] array,
			string delimeter = ", ",
			ConsoleColor color = ConsoleColor.Green,
			bool newLine = true)
		{
			if (array.Length == 0)
			{
				return;
			}

			var oldColor = Console.ForegroundColor;
			Console.ForegroundColor = color;

			Console.Write(array[0]);
			for (int i = 1; i < array.Length; i++)
			{
				Console.Write($"{delimeter}{array[i]}");
			}

			if (newLine)
			{
				Console.WriteLine();
			}

			Console.ForegroundColor = oldColor;
		}
	}
}
