﻿using System;

namespace Enums
{
	class Program
	{
		enum DayOfWeek
		{
			Monday = 1,
			Tuesday,
			Wednesday,
			Thursday,
			Friday,
			Saturday,
			Sunday
		}

		static void Main(string[] args)
		{
			Console.WriteLine("Input day number ");

			DayOfWeek day = (DayOfWeek)int.Parse(Console.ReadLine());

			Console.WriteLine($"You enter {day}");

			if (day == DayOfWeek.Saturday || day == DayOfWeek.Sunday)
			{
				Console.WriteLine("Weekend!!!!");
			}
		}
	}
}
