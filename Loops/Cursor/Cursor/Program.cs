﻿using System;

namespace Cursor
{
	class Program
	{
		static void Main(string[] args)
		{
			int x = 0;
			int y = 0;

			ConsoleKeyInfo keyInfo;
			do
			{
				keyInfo = Console.ReadKey();
				switch (keyInfo.Key)
				{
					case ConsoleKey.UpArrow:
						y--; break;
					case ConsoleKey.DownArrow:
						y++; break;
					case ConsoleKey.LeftArrow:
						x--; break;
					case ConsoleKey.RightArrow:
						x++; break;
				}
				if (x < 0)
					x = 0;
				if (y < 0)
					y = 0;
				if (x >= Console.WindowWidth)
					x--;
				if (y >= Console.WindowHeight)
					y--;
				Console.SetCursorPosition(x, y);
			} while (keyInfo.Key != ConsoleKey.Escape);
		}
	}
}
