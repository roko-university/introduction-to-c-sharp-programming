﻿using System;

namespace ContainsDigit
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Input number ");
			int number = int.Parse(Console.ReadLine());

			Console.Write("Input digit ");
			int digit = int.Parse(Console.ReadLine());

			if(digit >= 0 && digit <= 9)
			{
				int count = 0;
				int tempNumber = number;

				while (tempNumber > 0)
				{
					int mod = tempNumber % 10;
					if(mod == digit)
					{
						count++;
					}
					tempNumber = tempNumber / 10;
				}

				Console.WriteLine($"\"{number}\" contains {count} \"{digit}\"");
			}
			else
			{
				Console.WriteLine("Invalid values. Please try again.");
			}
		}
	}
}
