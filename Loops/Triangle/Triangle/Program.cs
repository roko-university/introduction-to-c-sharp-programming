﻿using System;

namespace Triangle
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Input triangle size");
			int n = int.Parse(Console.ReadLine());
			for (int i = 1; i <= n; i++)
			{
				for (int j = n; j > i; j--)
				{
					Console.Write(" ");
				}
				for (int j = 0; j < i; j++)
				{
					Console.Write("*");
				}
				Console.WriteLine();
			}
		}
	}
}
