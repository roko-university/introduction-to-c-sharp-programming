﻿using System;

namespace Strings
{
	class Program
	{
		static void Main(string[] args)
		{
			string s1 = "Hello!";
			string s2 = new string('*', 1000);
			string s3 = "";
			char[] chars = { 'H', 'e', 'l', 'l', 'o', '!' };
			string s4 = new string(chars);

			s3 = s1 + "!!"; // "Hello!!!"
			char c = s3[0]; // 'H'
			if (s1 == s4)
				Console.WriteLine("equals");
			else
				Console.WriteLine("not equals");

			Console.WriteLine(s1);
			Console.WriteLine(s2);
			Console.WriteLine(s3);
			Console.WriteLine(s4);
		}
	}
}
