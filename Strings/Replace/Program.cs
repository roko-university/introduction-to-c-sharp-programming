﻿using System;

namespace Replace
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Input string");
			string input = Console.ReadLine();

			string replacedString = ReplaceDigits(input);
			Console.WriteLine($"Without digits: {replacedString}");
		}

		static string ReplaceDigits(string input)
		{
			char[] array = input.ToCharArray();
			for (int i = 0; i < array.Length; i++)
			{
				if (char.IsDigit(array[i]))
					array[i] = '_';
			}
			return new string(array);
		}
	}
}
