﻿using System;
using System.Text;

namespace AdvancedReplace
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Input string");
			string input = Console.ReadLine();

			string replacedString = ReplaceNumbers(input);
			Console.WriteLine($"Without digits: {replacedString}");
		}

		// don't use this method in real program!
		static string ReplaceNumbers(string input)
		{
			string result = string.Empty;

			bool isNumberDetected = false;
			for (int i = 0; i < input.Length; i++)
			{
				if(char.IsDigit(input[i]))
				{
					if(!isNumberDetected)
					{
						isNumberDetected = true;
						result += '_';   // bad code!
					}
				}
				else
				{
					isNumberDetected = false;
					result += input[i];   // bad code!
				}
			}
			return result;
		}

		// don't use this method in real program!
		static string HowReplaceNumbersWorks(string input)
		{
			//string result = "";
			char[] result = new char[0];

			bool isNumberDetected = false;
			for (int i = 0; i < input.Length; i++)
			{
				if (char.IsDigit(input[i]))
				{
					if (!isNumberDetected)
					{
						isNumberDetected = true;

						//result += input[i];

						char[] newResult = new char[result.Length + 1];
						for (int j = 0; j < result.Length; j++)
						{
							newResult[j] = result[j];
						}
						
						newResult[newResult.Length - 1] = '_';
						//newResult[^1] = '_';  // C# 8.0+

						result = newResult;
					}
				}
				else
				{
					isNumberDetected = false;
					//result += input[i];   

					char[] newResult = new char[result.Length + 1];
					for (int j = 0; j < result.Length; j++)
					{
						newResult[j] = result[j];
					}
					newResult[newResult.Length - 1] = input[i];
					result = newResult;
				}
			}

			return new string(result);
		}

		static string ReplaceNumbersSB(string input)
		{
			StringBuilder sb = new StringBuilder();

			bool isNumberDetected = false;
			for (int i = 0; i < input.Length; i++)
			{
				if (char.IsDigit(input[i]))
				{
					if (!isNumberDetected)
					{
						isNumberDetected = true;
						sb.Append('_');
					}
				}
				else
				{
					isNumberDetected = false;
					sb.Append(input[i]);
				}
			}

			return sb.ToString();
		}
	}
}
