﻿using System;

namespace StringOperations
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Input string");
			string input = Console.ReadLine();

			int separators, punctuations, digits, letters;
			CalculateStatistic(input, out separators, out punctuations, out digits, out letters);

			string[] words = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);

			Console.WriteLine($"Your string contains {input.Length} symbols.");
			Console.WriteLine($"\t{words.Length} words;");
			Console.WriteLine($"\t{letters} letters;");
			Console.WriteLine($"\t{digits} digits;");
			Console.WriteLine($"\t{separators} separators;");
			Console.WriteLine($"\t{punctuations} punctuations;");

			Console.Write("Input string to search: ");
			string search = Console.ReadLine();

			int count = SubstringCount(input, search);
			Console.WriteLine($"Substring \"{search}\" was found {count} times");

			Console.Write("Input string to replace: ");
			string replace = Console.ReadLine();

			string result = input.Replace(search, replace);
			Console.WriteLine($"Replacement result: {result}");

			Console.Write("Input string to detect words start with...: ");
			string startText = Console.ReadLine();

			Console.WriteLine("Words found:");
			for (int i = 0; i < words.Length; i++)
			{
				if (words[i].StartsWith(startText, StringComparison.CurrentCultureIgnoreCase))
				{
					Console.WriteLine($"\t{words[i]}");
				}
			}
		}

		private static void CalculateStatistic(string input, out int separators, out int punctuations, out int digits, out int letters)
		{
			separators = 0;
			punctuations = 0;
			digits = 0;
			letters = 0;
			for (int i = 0; i < input.Length; i++)
			{
				if (char.IsSeparator(input[i]))
					separators++;
				if (char.IsPunctuation(input[i]))
					punctuations++;
				if (char.IsDigit(input[i]))
					digits++;
				if (char.IsLetter(input[i]))
					letters++;
			}
		}

		private static int SubstringCount(string input, string search)
		{
			int count = 0;
			int position = 0;

			input = input.ToLower();
			search = search.ToLower();

			do
			{
				position = input.IndexOf(search, position);
				if (position != -1)
				{
					count++;
					position++;
				}
			} while (position != -1);
			return count;
		}
	}
}
