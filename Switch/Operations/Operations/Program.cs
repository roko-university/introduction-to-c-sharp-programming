﻿using System;

namespace Operations
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Input first number ");
			int x = int.Parse(Console.ReadLine());

			Console.Write("Input operation ");
			string operation = Console.ReadLine();

			Console.Write("Input second number ");
			int y = int.Parse(Console.ReadLine());

			double result;

			switch (operation)
			{
				case "+":
					result = x + y;
					break;
				case "-":
					result = x - y;
					break;
				case "*":
					result = x * y;
					break;
				case "/":
					if(y == 0)
					{
						result = double.PositiveInfinity; // Not a number
						Console.WriteLine("You cannot divide by zero");
					}
					else
					{
						result = (double)x / y;
					}
					break;
				default:
					result = double.NaN; // Not a number
					Console.WriteLine("Invalid operation");
					break;
			}

			Console.WriteLine($"{x} {operation} {y} = {result}");
		}
	}
}
