﻿using System;

namespace Calc
{
	class Program
	{
		static void Main(string[] args)
		{
			int x;
			int y;
			int sum;

			x = int.Parse(Console.ReadLine());
			y = int.Parse(Console.ReadLine());

			sum = x + y;

			Console.WriteLine($"{x} + {y} = {sum}");
		}
	}
}
