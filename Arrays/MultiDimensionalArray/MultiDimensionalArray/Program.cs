﻿using System;

namespace MultiDimentionalArray
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Input place count ");
			int n = int.Parse(Console.ReadLine());
			Console.Write("Input row count ");
			int k = int.Parse(Console.ReadLine());
			Console.Write("Input column count ");
			int l = int.Parse(Console.ReadLine());

			double[,,] arr = new double[n, k, l];

			Random rnd = new Random();

			for (int i = 0; i < arr.GetLength(0); i++)
			{
				for (int j = 0; j < arr.GetLength(1); j++)
				{
					for (int v = 0; v < arr.GetLength(2); v++)
					{
						arr[i, j, v] = rnd.NextDouble();
					}
				}
			}

			for (int i = 0; i < arr.GetLength(0); i++)
			{
				for (int j = 0; j < arr.GetLength(1); j++)
				{
					for (int v = 0; v < arr.GetLength(2); v++)
					{
						Console.Write($"\t{arr[i, j, v]}");
					}
					Console.WriteLine();
				}
				Console.WriteLine("---------------------------------");
			}

			double summa = 0;
			for (int i = 0; i < arr.GetLength(0); i++)
			{
				for (int j = 0; j < arr.GetLength(1); j++)
				{
					for (int v = 0; v < arr.GetLength(2); v++)
					{
						summa += arr[i, j, v];
					}
				}
			}

			Console.WriteLine($"Summa of array items = {summa}");
		}
	}
}
