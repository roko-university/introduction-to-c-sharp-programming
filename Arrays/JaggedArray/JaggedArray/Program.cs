﻿using System;

namespace JaggedArray
{
	class Program
	{
		static void Main(string[] args)
		{
			int[][] arr;
			Console.WriteLine("Input row count");
			int n = int.Parse(Console.ReadLine());

			arr = new int[n][];

			Random rnd = new Random();

			for (int i = 0; i < arr.Length; i++)
			{
				Console.WriteLine($"Input colunts count for row[{i}]");
				int m = int.Parse(Console.ReadLine());
				arr[i] = new int[m];
				for (int j = 0; j < arr[i].Length; j++)
				{
					arr[i][j] = rnd.Next(0, 100);
				}
			}

			for (int i = 0; i < arr.Length; i++)
			{
				for (int j = 0; j < arr[i].Length; j++)
				{
					Console.Write($"{arr[i][j]}\t");
				}
				Console.WriteLine();
			}

			int summa = 0;
			for (int i = 0; i < arr.Length; i++)
			{
				for (int j = 0; j < arr[i].Length; j++)
				{
					summa += arr[i][j];
				}
			}

			Console.WriteLine($"Summa = {summa}");
		}
	}
}
