﻿using System;

namespace BasicArray
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Input array length");
			int n = int.Parse(Console.ReadLine());

			int[] arr = new int[n];

			Random rnd = new Random();

			for (int i = 0; i < arr.Length; i++)
			{
				arr[i] = rnd.Next(0, 100);
			}

			//for (int i = 0; i < arr.Length; i++)
			//{
			//	Console.Write($"Input arr[{i}] ");
			//	arr[i] = int.Parse(Console.ReadLine());
			//}

			//for (int i = 0; i < arr.Length; i++)
			//{
			//	arr[i] = i;
			//}

			//for (int i = 0; i < arr.Length; i++)
			//{
			//	Console.WriteLine(arr[i]);
			//}

			foreach (var item in arr)
			{
				Console.WriteLine(item);
			}

			int sum = 0;

			for (int i = 0; i < arr.Length; i++)
			{
				sum += arr[i];
			}

			Console.WriteLine($"Summa = {sum}");
		}
	}
}
