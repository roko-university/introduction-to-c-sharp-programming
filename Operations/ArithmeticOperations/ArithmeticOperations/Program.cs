﻿using System;

namespace _13._1.ArithmeticOperations
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Input x: ");
			double x;
			x = double.Parse(Console.ReadLine());

			int ix = (int)x;
			int rx = (int)Math.Round(x);
			
			double res = (double)ix / rx;

			Console.WriteLine($"x: {x}, (int)x: {ix}, round(x): {rx}");
			Console.WriteLine($"(int)x / round(x) = {res}");

			Console.WriteLine();
			Console.WriteLine($"sin(x) =\t{Math.Sin(x)}");
			Console.WriteLine($"round(x) =\t{Math.Round(x, 1)}");
			Console.WriteLine($"min(x, 1) =\t{Math.Min(x, 1)}");
			Console.WriteLine($"sqrt(x) =\t{Math.Sqrt(x)}");
			Console.WriteLine($"abs(x) =\t{Math.Abs(x)}");
			Console.WriteLine($"log(x) =\t{Math.Log(x)}");
			Console.WriteLine($"x*x*x*x =\t{Math.Pow(x, 4)}");
		}
	}
}
